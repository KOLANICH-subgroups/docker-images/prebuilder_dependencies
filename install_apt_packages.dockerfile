# syntax=docker/dockerfile:experimental
ARG base
FROM $base
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./fix_python_modules_paths.py ./
RUN \
  set -ex;\
  apt-get update;\
  apt-get install -y --no-install-recommends firejail firejail-profiles p7zip-full aria2 strip-nondeterminism fakeroot reprepro python3-gpg python3-apt dpkg-sig dpkg-dev lsb-release;\
  python3 ./fix_python_modules_paths.py;\
  apt-get autoremove --purge -y ;\
  apt-get clean && rm -rf /var/lib/apt/lists/* ;\
  rm -rf /tmp/*
