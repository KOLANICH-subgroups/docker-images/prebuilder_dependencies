# syntax=docker/dockerfile:experimental
ARG base
FROM $base
LABEL maintainer="KOLANICH"
WORKDIR /tmp
RUN \
  set -ex;\
  apt-get update;\
  apt-get install -y --no-install-recommends apparmor apparmor-utils;\
  if $( aa-enabled ) ;\
  then\
    aa-enforce /etc/apparmor.d/**;\
    apt-get install -y --no-install-recommends apparmor-profiles apparmor-profiles-extra;\
  else\
    apt-get remove -y apparmor apparmor-utils;\
  fi;\
  apt-get autoremove --purge -y ;\
  apt-get clean && rm -rf /var/lib/apt/lists/* ;\
  rm -rf /tmp/*
