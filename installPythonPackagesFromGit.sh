for repoURI in $(cat ./pythonPackagesToInstallFromGit.txt); do
  git clone --depth=1 $repoURI PKG_DIR;
  cd ./PKG_DIR;
  python3 ./setup.py bdist_wheel;
  pip3 install --upgrade --pre ./dist/*.whl;
  cd ..;
  rm -rf ./PKG_DIR;
done;
