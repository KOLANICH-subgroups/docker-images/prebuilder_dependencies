# syntax=docker/dockerfile:experimental
ARG base
FROM $base
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./pythonPackagesToInstallFromGit.txt ./
RUN \
  set -ex;\
  . ./installPythonPackagesFromGit.sh ;\
  pip3 install git+https://github.com/KOLANICH/python-patch@fixes;\
  python3 -m File2Package refresh dpkg;\
  rm -r ~/.cache/pip && true;\
  rm -rf /tmp/* && true
